# Copyright 2020-2024 src_prepare group
# Distributed under the terms of the GNU General Public License v2

import json
import re

import portage

from euscan import helpers, mangling, output

HANDLER_NAME = "launchpad"
CONFIDENCE = 100
PRIORITY = 90


def can_handle(pkg, url=None):
    return url and url.startswith("https://launchpad.net/")


def scan_url(pkg, url, options):
    "https://api.launchpad.net/1.0.html"

    match = re.search(
        r"https://launchpad.net/(?P<name>.*)/(?P<milestone>.*)/(?P<release>.*)/\+download/.*",
        url,
    )

    name = match.group("name")

    output.einfo(f"Using Launchpad API: {name}")

    request = helpers.urlopen(f"https://api.launchpad.net/1.0/{name}/releases")

    data = json.load(request)

    versions = [
        (release["version"], release["files_collection_link"])
        for release in data["entries"]
    ]

    cp, ver, rev = portage.pkgsplit(pkg.cpv)

    ret = []
    for up_pv, files_link in versions:
        pv = mangling.mangle_version(up_pv, options)
        if helpers.version_filtered(cp, ver, pv):
            continue
        request = helpers.urlopen(files_link)
        data = json.load(request)
        urls = " ".join(
            mangling.mangle_url(
                file["self_link"]
                .replace("https://api.launchpad.net/1.0/", "https://launchpad.net/")
                .replace("+file", "+download"),
                options,
            )
            for file in data["entries"]
            if file["file_type"] == "Code Release Tarball"
        )
        ret.append((urls, pv, HANDLER_NAME, CONFIDENCE))
    return ret
